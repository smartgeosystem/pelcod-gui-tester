import binascii
import datetime

from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import Qt, QSettings

from pelco_d_protocol import PelcoDProtocol
from rtsp_viewer import RtspViewer
from tcp_connector import TcpConnector
from udp_connector import UdpConnector


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        self.connector = None

        super(MainWindow, self).__init__()
        uic.loadUi('main_window.ui', self)

        self.settings = QSettings()

        self.actionRtspOpenViewer.triggered.connect(self.open_rtsp_viewer)

        self.rbDec.toggled.connect(self.set_num_format)

        self.btnConnect.clicked.connect(self.connect_to_dev)
        self.btnSendCustomCmd.clicked.connect(self.cmd_custom)

        self.btnStop.clicked.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnUp.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_UP))
        self.btnUp.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnDown.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_DOWN))
        self.btnDown.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnRight.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_RIGHT))
        self.btnRight.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnLeft.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_LEFT))
        self.btnLeft.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnZoomWide.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_ZOOM_WIDE))
        self.btnZoomWide.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnZoomTele.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_ZOOM_TELE))
        self.btnZoomTele.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnFocusNear.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_FOCUS_NEAR))
        self.btnFocusNear.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnFocusFar.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_FOCUS_FAR))
        self.btnFocusFar.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnIrisClose.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_IRIS_CLOSE))
        self.btnIrisClose.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnIrisOpen.pressed.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_IRIS_OPEN))
        self.btnIrisOpen.released.connect(lambda: self.cmd_standard(PelcoDProtocol.COMMAND_STOP))

        self.btnGoPreset.clicked.connect(self.cmd_preset)

        self.restore_settings()

        self.rtspViewer = None

    def closeEvent(self, event):
        if self.rtspViewer:
            self.rtspViewer.close()
            self.rtspViewer = None

    def open_rtsp_viewer(self):
        self.rtspViewer = RtspViewer()
        self.rtspViewer.show()

    def add_to_log(self, log_row):
        now_str = str(datetime.datetime.now().time())
        self.txtOutput.append('%s %s' % (now_str, log_row))

    def set_num_format(self, dec):
        spbs = [self.spbCmd1, self.spbCmd2, self.spbData1, self.spbData2]
        if dec:
            self.leSync.setText("255")
            for c in spbs:
                c.setDisplayIntegerBase(10)
        else:
            self.leSync.setText("FF")
            for c in spbs:
                c.setDisplayIntegerBase(16)

    def get_connection_type(self):
        return self.cmbConnectionType.currentText()

    def get_tcp_settings(self):
        return self.leHost.text(), self.spbPort.value(), self.spbTimeout.value()

    def get_udp_settings(self):
        return self.leHost.text(), self.spbPort.value(), self.spbTimeout.value()

    def restore_settings(self):
        self.leHost.setText(self.settings.value('last_host', '127.0.0.1'))
        self.spbPort.setValue(int(self.settings.value('last_port', 1000)))
        self.spbTimeout.setValue(int(self.settings.value('last_timeout', 3)))
        self.cmbConnectionType.setCurrentText(self.settings.value('last_conn_type', 'TCP'))


    def save_settings(self):
        self.settings.setValue('last_host', self.leHost.text())
        self.settings.setValue('last_port', self.spbPort.value())
        self.settings.setValue('last_timeout', self.spbTimeout.value())
        self.settings.setValue('last_conn_type', self.cmbConnectionType.currentText())

    def connect_to_dev(self):
        if self.get_connection_type() == 'TCP':
            self.connector = TcpConnector(self.get_tcp_settings()[:2], self.get_tcp_settings()[2])
            try:
                self.setCursor(Qt.WaitCursor)
                self.connector.connect()
                self.lblStatus.setText('<b>Connected</b>')
                self.add_to_log('Connected')
                self.save_settings()
            except Exception as ex:
                self.lblStatus.setText('<b>Disconnected</b>')
                self.add_to_log('Error on connect: ' + str(ex))
            finally:
                self.unsetCursor()
        if self.get_connection_type() == 'UDP':
            self.connector = UdpConnector(self.get_udp_settings()[:2], self.get_udp_settings()[2])
            try:
                self.setCursor(Qt.WaitCursor)
                self.connector.connect()
                self.lblStatus.setText('<b>Connected</b>')
                self.add_to_log('Connected')
                self.save_settings()
            except Exception as ex:
                self.lblStatus.setText('<b>Disconnected</b>')
                self.add_to_log('Error on connect: ' + str(ex))
            finally:
                self.unsetCursor()


    def send_cmd(self, cmd):
        try:
            if self.chkWaitAnswer.isChecked():
                resp = self.connector.send_command_with_answ(cmd)
                self.add_to_log('Command sent ' + cmd.hex())
                self.add_to_log('Response size (bytes): ' + str(len(resp)))
                self.add_to_log('Response: ' + binascii.hexlify(bytearray(resp)).decode('ascii'))
            else:
                self.connector.send_command(cmd)
                self.add_to_log('Command sent ' + cmd.hex())
        except Exception as ex:
            self.add_to_log('Error on send command: ' + str(ex))

    def cmd_standard(self, cmd_type):
        if cmd_type in PelcoDProtocol.MOVE_COMMANDS:
            cmd = PelcoDProtocol.generate_command(self.spbCamId.value(), cmd_type[0], cmd_type[1],
                                                  self.sldPanSpeed.value(), self.sldTiltSpeed.value())
        else:
            cmd = PelcoDProtocol.generate_command(self.spbCamId.value(), cmd_type[0], cmd_type[1], 0, 0)
        self.send_cmd(cmd)

    def cmd_preset(self):
        cmd = PelcoDProtocol.generate_command(self.spbCamId.value(), 0, 7, 0, self.spbPreset.value())
        self.send_cmd(cmd)

    def cmd_custom(self):
        cmd = PelcoDProtocol.generate_command(self.spbCamId.value(), self.spbCmd1.value(), self.spbCmd2.value(),
                                              self.spbData1.value(), self.spbData2.value())
        self.send_cmd(cmd)