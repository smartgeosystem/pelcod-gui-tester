
import socket


class TcpConnector:
    sock = None
    connected = False
    addr = ('localhost', 1000)
    timeout = 3

    def __init__(self, addr, timeout=3):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.addr = addr
        self.timeout = timeout

    def connect(self):
        self.sock.settimeout(self.timeout)
        self.sock.connect(self.addr)
        self.connected = True

    def send_command(self, command):
        self.sock.sendall(command)

    def send_command_with_answ(self, command):
        self.sock.sendall(command)
        resp = self.sock.recv(100)
        return resp
