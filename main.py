#!/usr/bin/python3
import sys

from PyQt5 import QtWidgets

from main_window import MainWindow


def main():
    app = QtWidgets.QApplication(sys.argv)
    mw = MainWindow()
    mw.show()
    app.exec()


if __name__ == '__main__':
    main()
