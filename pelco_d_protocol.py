
class PelcoDProtocol:

    COMMAND_STOP = (0, 0)
    COMMAND_RIGHT = (0, 2)
    COMMAND_LEFT = (0, 4)
    COMMAND_UP = (0, 8)
    COMMAND_DOWN = (0, 16)

    MOVE_COMMANDS = (
        COMMAND_RIGHT,
        COMMAND_LEFT,
        COMMAND_UP,
        COMMAND_DOWN
    )

    COMMAND_ZOOM_TELE = (0, 32)
    COMMAND_ZOOM_WIDE = (0, 64)
    COMMAND_FOCUS_FAR = (0, 128)
    COMMAND_FOCUS_NEAR = (1, 0)
    COMMAND_IRIS_OPEN = (2, 0)
    COMMAND_IRIS_CLOSE = (4, 0)

    @classmethod
    def generate_command(cls, addr: int, cmd1: int, cmd2: int, data1: int, data2: int):
        sym = (addr + cmd1 + cmd2 + data1 + data2) % 256
        return bytes([0xFF, addr, cmd1, cmd2, data1, data2, sym])

