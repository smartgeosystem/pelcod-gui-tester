from PyQt5 import QtWidgets, uic, QtCore
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QStyle

from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer


class RtspViewer(QtWidgets.QDialog):
    def __init__(self):
        self.connector = None

        super(RtspViewer, self).__init__()
        uic.loadUi('rtsp_viewer.ui', self)

        self.settings = QSettings()
        self.btnControl.pressed.connect(self.btn_pressed)

        self.player = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.player.setVideoOutput(self.owdgVideo)
        self.player.stateChanged.connect(self.on_media_state_changed)
        self.player.error.connect(self.on_media_error)

        self.leRtspUrl.setText(self.settings.value('last_rtsp_url', ''))
        self.update()

    def closeEvent(self, event):
        self.player.stop()

    def get_rtsp_url(self):
        return self.leRtspUrl.text()

    def btn_pressed(self):
        if self.player.state() == QMediaPlayer.StoppedState:
            self.player.setMedia(QMediaContent(QtCore.QUrl(self.get_rtsp_url())))
            self.player.play()
        elif self.player.state() == QMediaPlayer.PlayingState:
            self.player.stop()

    def on_media_state_changed(self):
        if self.player.state() == QMediaPlayer.PlayingState:
            self.btnControl.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
            self.lblStatus.setText('Playing')
            self.settings.setValue('last_rtsp_url', self.get_rtsp_url())
        else:
            self.btnControl.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
            self.lblStatus.setText('Stopped')


    def on_media_error(self):
        self.lblStatus.setText('Error: ' + self.player.errorString())

    #rtsp://94.20.63.126:554/axis-media/media.amp
    # media-playback-stop

